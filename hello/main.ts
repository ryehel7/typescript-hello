import { Point } from './Point';
import { Like } from './Like';

var message = 'hello world!';

let log = function (message) {
    console.log(message);
}

let doLog = (message) => {
    console.log(message);
}


function doSomething() {
    for (var i = 0; i <= 5; i++) {
        console.log(i);
    }
    console.log('finally ' + i);
}

function myNumb() {
    let a: number;
    a = 5;
    a = 7;
    //a = true;

    console.log(a);
}

function myNumbArr() {
    let a: number;
    let b: boolean;
    let c: string;
    let d: any;
    let e: number[] = [1, 2, 3, 4];
    let f: any[] = [1, true, 'dua', false];

    const colorRed = 0;

    enum Color { red, green, blue };

    let backgroundColor = Color.red;
    console.log(backgroundColor);

    enum ColorRGB { red = '255,0,0', green = '0,255,0', blue = '0,0,255' };

    console.log(ColorRGB.blue);

}

// function typeAssertion() {
//     let message;
//     message = 'abc';

//     let endWithC = (<string>message).endsWith('r');
//     console.log(endWithC);

//     let alternateEndWithC = (message as string).endsWith('c');
//     console.log(alternateEndWithC);
// }

function drawingWithInterface() {
    // ---------- Traditional prameter not good if you have much param -----//
    let drawPoint = (x, y) => {
        console.log(x, y);
    };
    drawPoint(1, 2);

    // ---end here--- //


    // --- object on params ---// 
    let drawPoint2 = (point: { x: number, y: number }) => {
        console.log(point.x, point.y);
    };

    drawPoint2({ x: 3, y: 5 });
    // --- end here --- //


    // --- with interface ---//
    interface Point {
        x: number;
        y: number;
    }

    let drawPoint3 = (point: Point) => {
        console.log(point.x, point.y);
    }

    drawPoint3({ x: 5, y: 9 });
}

let likes = new Like('Dislike');
likes.clickMe();
likes.clickMe();


//-----this is to call class POINT -----//
// let point = new Point(1, 2);
// point.x = 7;
// console.log(point.x);
// -- End -- //



//drawingWithInterface();
// typeAssertion();
// doLog(message);

//console.log(message);
//myNumb();
//myNumbArr();


// doSomething();

