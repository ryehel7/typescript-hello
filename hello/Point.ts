export class Point {
    // x: number;
    // y: number;
    // private time: number;

    // constructor(x?: number, y?: number) {
    //     this.x = x;
    //     this.y = y;
    // }

    private time: number;
    constructor(private _x?: number, private _y?: number) {

    }

    draw() {
        console.log('X : ' + this._x, 'Y : ' + this._y);
    }

    private getDistance(another: Point) {
        console.log(this.time);
    }

    get x() {
        return this._x;
    }

    set x(val) {
        if (val < 0) {
            throw new Error('Value less than 0');
        }
        else {
            this._x = val;
        }
    }
}

// let point = new Point(1, 2);
// point.x = 10;
// console.log(point.x);

