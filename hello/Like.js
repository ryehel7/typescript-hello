"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Like = /** @class */ (function () {
    function Like(_label, _like, _dislike) {
        this._label = _label;
        this._like = _like;
        this._dislike = _dislike;
        this._myfans = 0;
    }
    Like.prototype.clickMe = function () {
        if (this._label === 'Dislike') {
            this._label = 'Like';
            this._myfans++;
        }
        else {
            this._label = 'Dislike';
            this._myfans--;
        }
        console.log("Hey you " + this._label);
        console.log("My Fans " + this._myfans);
    };
    return Like;
}());
exports.Like = Like;
