"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Like_1 = require("./Like");
var message = 'hello world!';
var log = function (message) {
    console.log(message);
};
var doLog = function (message) {
    console.log(message);
};
function doSomething() {
    for (var i = 0; i <= 5; i++) {
        console.log(i);
    }
    console.log('finally ' + i);
}
function myNumb() {
    var a;
    a = 5;
    a = 7;
    //a = true;
    console.log(a);
}
function myNumbArr() {
    var a;
    var b;
    var c;
    var d;
    var e = [1, 2, 3, 4];
    var f = [1, true, 'dua', false];
    var colorRed = 0;
    var Color;
    (function (Color) {
        Color[Color["red"] = 0] = "red";
        Color[Color["green"] = 1] = "green";
        Color[Color["blue"] = 2] = "blue";
    })(Color || (Color = {}));
    ;
    var backgroundColor = Color.red;
    console.log(backgroundColor);
    var ColorRGB;
    (function (ColorRGB) {
        ColorRGB["red"] = "255,0,0";
        ColorRGB["green"] = "0,255,0";
        ColorRGB["blue"] = "0,0,255";
    })(ColorRGB || (ColorRGB = {}));
    ;
    console.log(ColorRGB.blue);
}
// function typeAssertion() {
//     let message;
//     message = 'abc';
//     let endWithC = (<string>message).endsWith('r');
//     console.log(endWithC);
//     let alternateEndWithC = (message as string).endsWith('c');
//     console.log(alternateEndWithC);
// }
function drawingWithInterface() {
    // ---------- Traditional prameter not good if you have much param -----//
    var drawPoint = function (x, y) {
        console.log(x, y);
    };
    drawPoint(1, 2);
    // ---end here--- //
    // --- object on params ---// 
    var drawPoint2 = function (point) {
        console.log(point.x, point.y);
    };
    drawPoint2({ x: 3, y: 5 });
    var drawPoint3 = function (point) {
        console.log(point.x, point.y);
    };
    drawPoint3({ x: 5, y: 9 });
}
var likes = new Like_1.Like('Dislike');
likes.clickMe();
likes.clickMe();
//-----this is to call class POINT -----//
// let point = new Point(1, 2);
// point.x = 7;
// console.log(point.x);
// -- End -- //
//drawingWithInterface();
// typeAssertion();
// doLog(message);
//console.log(message);
//myNumb();
//myNumbArr();
// doSomething();
