"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Point = /** @class */ (function () {
    function Point(_x, _y) {
        this._x = _x;
        this._y = _y;
    }
    Point.prototype.draw = function () {
        console.log('X : ' + this._x, 'Y : ' + this._y);
    };
    Point.prototype.getDistance = function (another) {
        console.log(this.time);
    };
    Object.defineProperty(Point.prototype, "x", {
        get: function () {
            return this._x;
        },
        set: function (val) {
            if (val < 0) {
                throw new Error('Value less than 0');
            }
            else {
                this._x = val;
            }
        },
        enumerable: true,
        configurable: true
    });
    return Point;
}());
exports.Point = Point;
// let point = new Point(1, 2);
// point.x = 10;
// console.log(point.x);
